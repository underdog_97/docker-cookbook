## Install Docker

## In project root in terminal run `npm i`
## In project root in terminal run `cd packages/backend` and `npm i`
## In project root in terminal run `cd packages/frontend` and `npm i`
## In project root in terminal run `docker-compose up`

## In browser use localhost:4200 to open frontend and localhost:3000 to open backend
