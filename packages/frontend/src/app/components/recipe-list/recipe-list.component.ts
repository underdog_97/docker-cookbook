import { Component, OnInit } from '@angular/core';
import { DataTransferService } from 'src/app/services/data-transfer.service';

@Component({
  selector: 'app-recipe-list',
  templateUrl: './recipe-list.component.html',
  styleUrls: ['./recipe-list.component.scss']
})
export class RecipeListComponent implements OnInit {

  currentPage = 1;
  recipes: any = [];
  itemsOnPage = 4;

  constructor(private dts: DataTransferService) { }

  ngOnInit(): void {
    this.dts.getRecipes().subscribe(data => this.recipes = data.result);
  }

  pagesLength(array) {
    const numArray = [];
    for (let i = 1; i <= Math.ceil(array.length / this.itemsOnPage); i++) {
      numArray.push(i);
    }
    return numArray;
  }

  changePage(index) {
    this.currentPage = index;
    window.scroll({
      top: 0,
      left: 0,
      behavior: 'smooth'
    });
  }

  nextPage() {
    this.currentPage += 1;
    window.scroll({
      top: 0,
      left: 0,
      behavior: 'smooth'
    });
  }

  previousPage() {
    this.currentPage -= 1;
    window.scroll({
      top: 0,
      left: 0,
      behavior: 'smooth'
    });
  }

  incrementItems() {
    this.itemsOnPage += 1;
    this.currentPage = 1;
  }

  decrementItems() {
    this.itemsOnPage -= 1;
    this.currentPage = 1;
  }

}
