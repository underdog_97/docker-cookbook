import { Component, OnInit } from '@angular/core';
import { DataTransferService } from '../../services/data-transfer.service';
import { Recipe } from '../../models/Recipe';

@Component({
  selector: 'app-main',
  templateUrl: './main.component.html',
  styleUrls: ['./main.component.scss']
})
export class MainComponent implements OnInit {

  recipes =  [] as any;
  currentRecipe: Recipe;

  constructor(private dts: DataTransferService) { }

  ngOnInit(): void {
    this.dts.getRecipes()
    .subscribe((data) => {
      this.recipes = data;
      this.currentRecipe = this.recipes.result[0];
    });
  }

  getImageUrl(item) {
    return `url('${item.imageUrl}')`;
  }

  changeRecipe(index) {
    this.currentRecipe = this.recipes.result[index];
  }

}
