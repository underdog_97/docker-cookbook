import { Component, OnInit } from '@angular/core';
import { FormGroup, FormControl, Validators } from '@angular/forms';
import { DialogTransferVariablesService } from '../../services/dialog-transfer-variables.service';
import { ActivatedRoute, Router } from '@angular/router';
import { MatSnackBar } from '@angular/material/snack-bar';
import { CdkDragDrop, moveItemInArray, transferArrayItem } from '@angular/cdk/drag-drop';

import {
  trigger,
  state,
  style,
  animate,
  transition,
  // ...
} from '@angular/animations';
import { DataTransferService } from 'src/app/services/data-transfer.service';

@Component({
  selector: 'app-add-recipe',
  templateUrl: './add-recipe.component.html',
  styleUrls: ['./add-recipe.component.scss'],
  animations: [
    trigger('openMenu', [
      // ...
      state('open', style({
        transform: 'translateX(100%)',
        overflow: 'hidden',
      })),
      state('closed', style({
      })),
      transition('open => closed', [
        animate('0.3s')
      ]),
      transition('closed => open', [
        animate('0.5s')
      ]),
    ]),
  ]
})
export class AddRecipeComponent implements OnInit {

  ingredients = [];
  steps = [];
  imageURL = 'http://localhost:3000/images/default.jpg';

  profileForm = new FormGroup({
    recipeName: new FormControl('', [Validators.required]),
    description: new FormControl(''),
    ingredient: new FormControl(''),
    step: new FormControl(''),
    time: new FormControl(''),
    avatar: new FormControl(null),
  });

  constructor(private dtv: DialogTransferVariablesService,
              private dts: DataTransferService,
              private router: Router,
              private snackBar: MatSnackBar) { }

  ngOnInit(): void {
  }

  async submitForm() {
    let id: string;
    const body = {
      name: this.profileForm.value.recipeName,
      description: this.profileForm.value.description,
      time: this.profileForm.value.time,
      steps: this.steps,
      ingredients: this.ingredients
    };
    this.dts.postRecipe(body).subscribe(result => {
      id = result.result;
      if (this.profileForm.value.avatar !== null) {
        this.dts.uploadImage(this.profileForm.value.avatar, id)
          .subscribe();
      }
      this.router.navigate(['../recipeList', id]);
    });
  }

  pushIngred() {
    if (this.profileForm.value.ingredient) {
      this.ingredients.push(this.profileForm.value.ingredient);
      this.profileForm.controls.ingredient.setValue('');
    }
  }

  deleteIng(i) {
    this.ingredients.splice(i, 1);
  }

  pushStep() {
    if (this.profileForm.value.step) {
      this.steps.push(this.profileForm.value.step);
      this.profileForm.controls.step.setValue('');
    }
  }

  deleteStep(i) {
    this.steps.splice(i, 1);
  }

  drop(event: CdkDragDrop<string[]>) {
    moveItemInArray(this.ingredients, event.previousIndex, event.currentIndex);
  }

  dropStep(event: CdkDragDrop<string[]>) {
    moveItemInArray(this.steps, event.previousIndex, event.currentIndex);
  }

  showPreview(event) {
    const file = (event.target as HTMLInputElement).files[0];
    this.profileForm.patchValue({
      avatar: file
    });
    this.profileForm.get('avatar').updateValueAndValidity();

    // File Preview
    const reader = new FileReader();
    reader.onload = () => {
      this.imageURL = reader.result as string;
    };
    reader.readAsDataURL(file);
  }

}
