import { Component, OnInit, Inject } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { DataTransferService } from 'src/app/services/data-transfer.service';
import { RouterModule, Router } from '@angular/router';
import { MatDialog, MatDialogRef, MAT_DIALOG_DATA} from '@angular/material/dialog';
import { MatSnackBar } from '@angular/material/snack-bar';

@Component({
  selector: 'app-recipe',
  templateUrl: './recipe.component.html',
  styleUrls: ['./recipe.component.scss']
})
export class RecipeComponent implements OnInit {

  routeId: string;
  recipe: any;


  constructor(private activatedroute: ActivatedRoute,
              private dts: DataTransferService,
              private router: Router,
              public dialog: MatDialog,
              private snackBar: MatSnackBar) { }

  openDialog(item): void {
    const dialogRef = this.dialog.open(OpenHistoryDialogComponent, {
      width: '30%',
      data: item
    });
  }

  ngOnInit(): void {
    this.routeId = this.activatedroute.snapshot.paramMap.get('id');
    this.dts.getRecipeById(this.routeId)
      .subscribe(result => {
        this.recipe = result.recipe;
      });

  }

  delete(id) {
    this.dts.deleteRecipe(id).subscribe(result => {
      this.openSnackBar('Deleted!');
    });
    this.router.navigate(['/recipeList']);
  }

  deleteImage(id) {
    this.dts.deleteImage(id).subscribe(result => {
      this.openSnackBar('Deleted!');
    });
  }

  openSnackBar(message) {
    this.snackBar.open(message, 'ok', {
      duration: 3000,
    });
  }

}
@Component({
  selector: 'app-open-history',
  templateUrl: 'open-history.html',
})
export class OpenHistoryDialogComponent {

  constructor(
    public dialogRef: MatDialogRef<OpenHistoryDialogComponent>,
    @Inject(MAT_DIALOG_DATA) public data) {}

  onNoClick(): void {
    this.dialogRef.close();
  }

}
