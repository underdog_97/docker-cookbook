import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { DataTransferService } from 'src/app/services/data-transfer.service';
import { MatSnackBar } from '@angular/material/snack-bar';

@Component({
  selector: 'app-edit-recipe',
  templateUrl: './edit-recipe.component.html',
  styleUrls: ['./edit-recipe.component.scss']
})
export class EditRecipeComponent implements OnInit {

  constructor(private activatedroute: ActivatedRoute,
              private dts: DataTransferService,
              private router: Router,
              private snackBar: MatSnackBar) { }

  routeId: string;
  recipe: any;

  ngOnInit(): void {
    this.routeId = this.activatedroute.snapshot.paramMap.get('id');
    this.dts.getRecipeById(this.routeId)
      .subscribe(result => {
        this.recipe = result.recipe;
      });
  }

  openSnackBar(message) {
    this.snackBar.open(message, 'ok', {
      duration: 3000,
    });
  }

  save() {
    this.dts.upgradeById(this.recipe._id, this.recipe).subscribe(result => this.openSnackBar(result.message));
    this.router.navigate(['../../recipeList', this.recipe._id]);
  }

}
