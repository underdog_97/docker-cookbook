import { TestBed } from '@angular/core/testing';

import { DialogTransferVariablesService } from './dialog-transfer-variables.service';

describe('DialogTransferVariablesService', () => {
  let service: DialogTransferVariablesService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(DialogTransferVariablesService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
