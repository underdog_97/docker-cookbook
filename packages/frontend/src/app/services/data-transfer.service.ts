import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Recipe } from '../models/Recipe';
@Injectable({
  providedIn: 'root'
})
export class DataTransferService {

  getAll = 'http://localhost:3000/recipes';
  getById = 'http://localhost:3000/recipes';
  create = 'http://localhost:3000/recipes/create';
  deleteById = 'http://localhost:3000/recipes';
  putById = 'http://localhost:3000/recipes';
  deleteImageById = 'http://localhost:3000/recipes/deleteImage';

  constructor(private http: HttpClient) { }

  getRecipes() {
    return this.http.get<any>(this.getAll);
  }

  getRecipeById(id) {
    return this.http.get<Recipe>(`${this.getById}/${id}`);
  }

  postRecipe(body) {
    return this.http.post<any>(this.create, body);
  }

  deleteRecipe(id) {
    return this.http.delete(`${this.deleteById}/${id}`);
  }

  upgradeById(id, body) {
    return this.http.put<any>(`${this.putById}/${id}`, body);
  }

  deleteImage(id) {
    return this.http.delete<Recipe>(`${this.deleteImageById}/${id}`)
  }

  public uploadImage(image: File, id: string) {
    const formData = new FormData();

    formData.append('avatar', image);
    formData.append('id', id);
    
    return this.http.post('http://localhost:3000/recipes/upload', formData);
  }

}
