import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { HttpClientModule } from '@angular/common/http';
import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { MainComponent } from './components/main/main.component';
import { RecipeComponent } from './components/recipe/recipe.component';
import { AddRecipeComponent } from './components/add-recipe/add-recipe.component';
import { MenuComponent } from './components/menu/menu.component';
import { RecipeListComponent } from './components/recipe-list/recipe-list.component';
import { ReactiveFormsModule } from '@angular/forms';
import { MatSliderModule } from '@angular/material/slider';
import { MatTabsModule } from '@angular/material/tabs';
import { MatInputModule } from '@angular/material/input';
import { MatIconModule } from '@angular/material/icon';
import { MatButtonModule } from '@angular/material/button';
import { MatListModule } from '@angular/material/list';
import { DragDropModule } from '@angular/cdk/drag-drop';
import { MatCardModule } from '@angular/material/card';
import { MatMenuModule } from '@angular/material/menu';
import { MatToolbarModule } from '@angular/material/toolbar';
import { MatSidenavModule } from '@angular/material/sidenav';
import { PagesPipe } from './pipes/pages.pipe';
import { EditRecipeComponent } from './components/recipe/edit-recipe/edit-recipe.component';
import { FormsModule } from '@angular/forms';
import { MatDialogModule } from '@angular/material/dialog';
import { OpenHistoryDialogComponent } from '../app/components/recipe/recipe.component';
import { MatSnackBarModule } from '@angular/material/snack-bar';

const angularMaterialModules = [ MatSliderModule,
  MatTabsModule,
  MatInputModule,
  MatIconModule,
  MatButtonModule,
  MatListModule,
  DragDropModule,
  MatCardModule,
  MatMenuModule,
  MatToolbarModule,
  MatSidenavModule,
  FormsModule,
  MatDialogModule,
  MatSnackBarModule,
];

@NgModule({
  declarations: [
    AppComponent,
    MainComponent,
    RecipeComponent,
    AddRecipeComponent,
    MenuComponent,
    RecipeListComponent,
    PagesPipe,
    EditRecipeComponent
  ],
  entryComponents: [
    OpenHistoryDialogComponent,
  ],
  imports: [
    BrowserModule,
    DragDropModule,
    AppRoutingModule,
    HttpClientModule,
    BrowserAnimationsModule,
    ReactiveFormsModule,
    ...angularMaterialModules,
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
