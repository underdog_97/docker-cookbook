import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { RecipeComponent } from './components/recipe/recipe.component';
import { MainComponent } from './components/main/main.component';
import { AddRecipeComponent } from './components/add-recipe/add-recipe.component';
import { RecipeListComponent } from './components/recipe-list/recipe-list.component';
import { EditRecipeComponent } from './components/recipe/edit-recipe/edit-recipe.component';

const routes: Routes = [
  { path: '', component: RecipeListComponent },
  { path: 'recipeList/:id', component: RecipeComponent },
  { path: 'addRecipe', component: AddRecipeComponent },
  { path: 'editRecipe/:id', component: EditRecipeComponent },
  { path: 'recipeList', component: RecipeListComponent },
  { path: '**', component: RecipeListComponent }
]

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
