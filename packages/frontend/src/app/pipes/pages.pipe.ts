import { Pipe, PipeTransform } from '@angular/core';

@Pipe({
  name: 'pages'
})
export class PagesPipe implements PipeTransform {

  transform(value: any[], ...args: any): unknown {

    const startElem = args[0] * args[1] - args[1];
    const endElem = args[0] * args[1];
    return value.slice(startElem, endElem);
  }

}
