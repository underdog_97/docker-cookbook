export interface Recipe {
  recipe: any;
  _id: string;
  name: string;
  ingredients: [string];
  steps: [string];
  description: string;
  lastUpdateDate: Date;
  imageUrl: string;
  historyPath: OldRecipeVariant;
}

export interface OldRecipeVariant {
  name: string;
  ingredients: [string];
  steps: [string];
  description: string;
  invalidateDate: Date;
}
