// Transpile all code following this line with babel and use '@babel/preset-env' (aka ES6) preset.
// eslint-disable-next-line import/no-extraneous-dependencies
require('babel-core/register');
// eslint-disable-next-line import/no-extraneous-dependencies
require('babel-polyfill');

// Import the rest of our application.
module.exports = require('./server.js');
