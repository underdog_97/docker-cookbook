import mongoose from 'mongoose';

const OldRecipeVariant = mongoose.Schema({
  name: String,
  description: String,
  invalidateDate: Date,
  time: String,
  ingredients: [String],
  steps: [String],
});

const RecipeSchema = mongoose.Schema({
  _id: mongoose.Schema.Types.ObjectId,
  name: String,
  description: String,
  lastUpdateDate: Date,
  imageUrl: String,
  time: String,
  ingredients: [String],
  steps: [String],
  historyPath: [OldRecipeVariant],
});

export const RecipeModel = mongoose.model('Recipe', RecipeSchema);
export const OldRecipeModel = mongoose.model('OldRecipe', OldRecipeVariant);
