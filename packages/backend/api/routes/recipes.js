import express from 'express';
import {
  getAll, deleteById, create, updateById, getById, fileUpload, deleteImageById,
} from '../controllers/recipes';

const router = express.Router();

router.post('/create', create);
router.post('/upload', fileUpload);
router.get('/', getAll);
router.get('/:id', getById);
router.delete('/:id', deleteById);
router.delete('/deleteImage/:id', deleteImageById);
router.put('/:id', updateById);

export default router;
