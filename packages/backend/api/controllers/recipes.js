import mongoose from 'mongoose';
import { RecipeModel as Recipe, OldRecipeModel as OldRecipe } from '../models/recipe';

export const getAll = async (req, res) => {
  let status = 200;
  try {
    const docs = await Recipe.find();
    res.status(status).json({ result: docs });
  } catch (e) {
    status = 404;
    res.status(status).json({ e });
  }
};

export const create = async (req, res) => {
  let status = 404;
  const imageUrl = 'http://localhost:3000/images/default.jpg';
  try {
    const recipeId = new mongoose.Types.ObjectId();
    const recipe = new Recipe({
      _id: recipeId,
      name: req.body.name,
      description: req.body.description,
      ingredients: req.body.ingredients,
      steps: req.body.steps,
      time: req.body.time,
      lastUpdateDate: new Date(),
      imageUrl,
      historyPath: [],
    });
    await recipe.save();
    res.status(200).json({ result: recipeId });
  } catch (e) {
    status = 404;
    res.status(status).json({
      error: e,
    });
  }
};

export const getById = async (req, res) => {
  let status = 200;
  const findedRecipe = await Recipe.findOne({ _id: req.params.id });
  try {
    res.status(status).json({
      recipe: findedRecipe,
    });
  } catch (e) {
    status = 404;
    res.status(status).json({
      error: e,
    });
  }
};

export const updateById = async (req, res) => {
  let status = 200;
  const message = `Recipe with ID:"${req.params.id}" updated!`;
  try {
    const findedRecipe = await Recipe.findOne({ _id: req.params.id });
    const oldRecipe = await new OldRecipe({
      name: findedRecipe.name,
      description: findedRecipe.description,
      invalidateDate: findedRecipe.lastUpdateDate,
      time: findedRecipe.time,
      ingredients: findedRecipe.ingredients,
      steps: findedRecipe.steps,
    });
    await Recipe.updateOne({
      _id: req.params.id,
    },
    {
      name: req.body.name || findedRecipe.name,
      description: req.body.description || findedRecipe.description,
      lastUpdateDate: new Date(),
      time: req.body.time || findedRecipe.time,
      steps: req.body.steps || findedRecipe.steps,
      ingredients: req.body.ingredients || findedRecipe.ingredients,
      imageUrl: findedRecipe.imageUrl,
      historyPath: [...findedRecipe.historyPath, oldRecipe],
    });
    await res.status(status).json({ message });
  } catch (e) {
    status = 404;
    res.status(status).json({
      error: e,
    });
  }
};

export const deleteById = async (req, res) => {
  let status = 404;
  let message = 'Recipe with such id doesn\'t exist';
  try {
    const recipe = await Recipe.findOne({ _id: req.params.id });
    if (recipe) {
      await recipe.deleteOne({ _id: req.params.id });
      status = 200;
      message = `Successful deleted. ID: ${req.params.id}`;
    }
    await res.status(status).json({ message });
  } catch (e) {
    res.status(status).json({
      error: e,
    });
  }
};

// async func to upload or replace image by recipeID.
export const fileUpload = async (req, res) => {
  //   let imageUrl = 'http://localhost:3000/images/default.jpg';
  const imageUrl = `http://localhost:3000/images/${req.files.avatar.name}`;
  try {
    // || !req.body.recipeId
    if (!req.files) {
      res.status(404).json({
        message: 'No file to upload or recipeId not entered!',
      });
      return false;
    }
    // file will name with text '+temp' to avoid conflict with delete function.
    await req.files.avatar.mv(`./public/images/${req.files.avatar.name}`, (e) => {
      if (e) return res.status(500).send(e);
    });
    await Recipe.updateOne({
      _id: req.body.id,
    },
    {
      imageUrl: `http://localhost:3000/images/${req.files.avatar.name}`,
    });
    await res.status(200).json({
      message: req.body.id,
      url: `http://localhost:3000/images/${req.files.avatar.name}`,
    });
  } catch (e) {
    res.status(404).json({
      error: e,
    });
    return false;
  }
};

export const deleteImageById = async (req, res) => {
  const defaultImageUrl = 'http://localhost:3000/images/default.jpg';
  const recipeId = req.params.id;
  const status = 200;
  await Recipe.updateOne({
    _id: recipeId,
  },
  {
    imageUrl: defaultImageUrl,
  });

  await res.status(status).json({
    message: defaultImageUrl,
    id: recipeId,
  });
};
