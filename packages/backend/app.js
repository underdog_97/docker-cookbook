import express from 'express';
import morgan from 'morgan';
import cors from 'cors';
import mongoose from 'mongoose';
import bodyParser from 'body-parser';
import fileUpload from 'express-fileupload';

import recipeRoutes from './api/routes/recipes';

const app = express();

mongoose.connect('mongodb://cookcook:123123@db').then(() => {
  console.log('Connected to MongoDB');
}).catch((err) => {
  console.log('Not Connected to Database ERROR', err);
});

app.use(cors());
app.use(morgan('dev'));
app.use(bodyParser.urlencoded({ extended: false }));
app.use(bodyParser.json());
app.use(fileUpload());
app.use(express.static('public'));

app.use('/recipes', recipeRoutes);

export default app;
